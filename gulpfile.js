'use strict';

var

  theme_name    = 'bigprs-official' + '/',
// defining theme location
theme_directory = {
    source          :   './source/',
    dest            :   '../' + theme_name
},


gulp          = require('gulp'),
gutil         = require('gulp-util'),
newer         = require('gulp-newer'),
imagemin      = require('gulp-imagemin'),
sassPlugin    = require('gulp-sass'),
postcss       = require('gulp-postcss'),
deporder      = require('gulp-deporder'),
concat        = require('gulp-concat'),
stripdebug    = require('gulp-strip-debug'),
uglify        = require('gulp-uglify'),
cache         = require('gulp-cache'),
gulpIf        = require('gulp-if'),
del           = require('del'),
wpPot         = require('gulp-wp-pot'),
rtlcss        = require('rtlcss'),
rename        = require('gulp-rename'),
pump          = require( 'pump' ),
svg2png       = require( 'gulp-svg2png' ),

images        = {
  source        : [theme_directory.source + 'images/*.+(png|svg)', theme_directory.source + 'images/png/**/*'],
  dest          : theme_directory.dest   + 'images/',
  pngSource     : theme_directory.source + 'images/png/**/*.svg'
},

sass          = {
  source        : theme_directory.source  + 'sass/**/*.+(scss|sass)',
  dest          : theme_directory.dest,
  sassOptions          : {
    outputStyle     : 'dested',
    imagePath       : images.dsst,
    procision       : 3,
    errLogToConsole : true
  },
  processors    : [
    require('postcss-assets')({
      loadPaths     : [theme_directory.source + 'images/'],
      basePath      : theme_directory.dest,
      baseUrl       : '/wp-content/themes/' + theme_name
    }),
    require('autoprefixer')({
      browsers      : ['last 2 versions', '> 1%']
    }),
    require('css-mqpacker'),
    require('cssnano')
  ]
},

js            = {
  source        : [
    theme_directory.source  + 'js/public/**/*',
    theme_directory.source  + 'js/admin/**/*',
    theme_directory.source  + 'js/localize/**/*'
  ],
  dest          : theme_directory.dest    + 'js/',
  filename      : [
    'script.min.js',
    'settings.min.js',
    'localize.js'
  ],
  type        : '*.js'
},

files           = {
  source        : theme_directory.source    + 'files/**/*',
  dest          : theme_directory.dest
},

syncOptions   = {
  // host          : '192.168.42.24',
  proxy         : 'localhost',
  files         : theme_directory.dest + '**/*',
  open          : false,
  notify        : false,
  ghostMode     : false,
  ui            : {
    port        : 8001
  }
},

fonts         = {
  source        : theme_directory.source    + 'fonts/**/*',
  dest          : theme_directory.dest      + 'fonts/'
},

pot           = {
  source        : files.source,
  dest          : theme_directory.dest      + 'languages/bigprs-official.pot'
},

rmEn        = {
  source        : theme_directory.dest      + '**/*.css',
  dest          : theme_directory.dest,
  options       : {
    dirname     : 'ltr',
    prefix      : 'en-',
    extname     : '.css'
  }
},

ltr         = {
  source        : theme_directory.dest      + 'ltr/**/*.css',
  dest          : theme_directory.dest      + 'ltr/',
}
;

var browserSync = false;

gulp.task('rm-en', () =>
  gulp.src(rmEn.source)
      .pipe(rename(rmEn.options))
      .pipe(gulp.dest(rmEn.dest))
);

gulp.task('wpot', () =>

  gulp.src(pot.source)
      .pipe(wpPot( {
        domain      : 'bigprs-official',
        package     : 'bigprs-official'
      } ).on('error', function() { return gutil.log }))
      .pipe(gulp.dest(pot.dest))
);

gulp.task('browserSync', () => {
  if ( browserSync === false ) {
    browserSync = require('browser-sync').create();
    browserSync.init(syncOptions);
  }
});
// develop with compression
gulp.task('default', ['build', 'syncWatch']);

// develop without compression
gulp.task('dev', ['devBuild', 'devSyncWatch']);

// build without compression
gulp.task('devBuild', ['svg2png', 'minimage', 'files', 'wpot', 'devsass', 'clean:fonts', 'font', 'devjs']);

// build with strip debug codes
gulp.task('publish', ['files', 'wpot', 'sass', 'clean:fonts', 'font', 'stripjsdebug']);

// build with debuging code
gulp.task('build', ['files', 'wpot', 'sass', 'clean:fonts', 'font', 'minjs']);

// watch for develope with no compression
gulp.task('devSyncWatch', ['browserSync'], () => {
  //gulp.watch(files.source, (browserSync !== false ? browserSync.reload : {}));
  // gulp.watch(images.source, ['devimage']);
  gulp.watch(files.source, ['files'], function() {
    (browserSync !== false ? browserSync.reload(function(err, bs) {
      console.log(bs.options.urls.local);
    }) : {})
  });
  gulp.watch(fonts.source, ['clean:fonts', 'font']);
  gulp.watch(sass.source, ['devsass']);
  gulp.watch(js.source, ['devjs']);
  gulp.watch(pot.source, ['wpot']);
});

// watch for develop with compression and browserSync
gulp.task('syncWatch', ['browserSync'], () => {
  gulp.watch(files.source, ['files'], function() {
    (browserSync !== false ? browserSync.reload(function(err, bs) {
      console.log(bs.options.urls.local);
    }) : {})
  });
  // gulp.watch(images.source, ['minimage']);
  gulp.watch(fonts.source, ['clean:fonts', 'font']);
  gulp.watch(sass.source, ['sass']);
  gulp.watch(js.source, ['minjs']);
  gulp.watch(pot.source, ['wpot']);
});

// just watch for change files without browserSync
gulp.task('watch', function() {
  gulp.watch(images.source, ['minimage']);
  gulp.watch(fonts.source, ['clean:fonts', 'font']);
  gulp.watch(sass.source, ['sass']);
  gulp.watch(js.source, ['minjs']);
  gulp.watch(pot.source, ['wpot']);
});

gulp.task('files', ['minimage'], () =>
  gulp.src(files.source)
    .pipe(newer(files.dest))
    .pipe(gulp.dest(files.dest).on('error', function() { gutil.log }))
);

gulp.task( 'svg2png', function(cb) {
  pump([
    gulp.src( images.pngSource ),
    newer(images.pngSource),
    gulpIf( '*.svg', svg2png() ),
    // .pipe(imagemin({ interlaced: true })
    gulp.dest(images.dest)

  ], cb);
});

// compress new images
gulp.task('minimage', () =>
  gulp.src(images.source)
      .pipe(newer(images.dest))
      .pipe(imagemin({ interlaced: true }))
      // .pipe(cache(imagemin({
      //   interlaced: true
      // })))
      .pipe(gulp.dest(images.dest))
);

// copy new images without compression
gulp.task('devimage', () => 
  gulp.src(images.source)
      .pipe(newer(images.dest))
      .pipe(gulp.dest(images.dest))
);

// copy new images without compression
gulp.task('font', () => 
  gulp.src(fonts.source)
      .pipe(newer(fonts.dest))
      .pipe(gulp.dest(fonts.dest))
);

// clear used cache
gulp.task('cache:clear', (callback) => 
  cache.clearAll(callback)
);

// translate sass files to css with minify
gulp.task('sass', ['minimage'], () =>
  gulp.src(sass.source)
    .pipe(sassPlugin(sass.sassOptions).on('error', sassPlugin.logError))
    .pipe(postcss(sass.processors))
    .pipe(gulp.dest(sass.dest))
    .pipe((browserSync !== false ? browserSync.reload({stream: true}, function(err, bs) {
      console.log(bs.options.urls.local);
    }) : gutil.noop()))
);

// just translate sass files
gulp.task('devsass', ['minimage'], () => 
  gulp.src(sass.source)
    .pipe(sassPlugin(sass.sassOptions).on('error', sassPlugin.logError))
    .pipe(gulp.dest(sass.dest))
    .pipe((browserSync !== false ? browserSync.reload({stream: true}, function(err, bs) {
      console.log(bs.options.urls.local);
    }) : gutil.noop()))
);
// minify js files into one file
// this code errored for too many times callback function called
// gulp.task('minjs', (cb) => {
//   for (var i = 0 ; i < js.source.length; i ++) {
//     pump( [
//       gulp.src(js.source[i]),
//       deporder(),
//       concat(js.filename[i]),
//       gulpIf(js.type, uglify()),
//       gulp.dest(js.dest)
//     ],cb );
//   }
// });
gulp.task('minjs', ['publicjs', 'adminjs', 'localizejs']);

gulp.task('publicjs', (cb) => {
  pump( [
    gulp.src(js.source[0]),
    deporder(),
    concat(js.filename[0]),
    gulpIf(js.type, uglify()),
    gulp.dest(js.dest),

    (browserSync !== false ? browserSync.reload({stream: true},function(err, bs) {
      console.log(bs.options.urls.local);
    }) : gutil.noop())

  ], cb );
});
gulp.task('adminjs', (cb) => {
  pump( [
    gulp.src(js.source[1]),
    deporder(),
    concat(js.filename[1]),
    gulpIf(js.type, uglify()),
    gulp.dest(js.dest),

    (browserSync !== false ? browserSync.reload({stream: true},function(err, bs) {
      console.log(bs.options.urls.local);
    }) : gutil.noop())

  ], cb );
});
gulp.task('localizejs', (cb) => {
  pump( [
    gulp.src(js.source[2]),
    deporder(),
    concat(js.filename[2]),
    gulpIf(js.type, uglify()),
    gulp.dest(js.dest),
  ], cb );
});

// clear images folder for new images
gulp.task('clean:image', () => {
    return del.sync(images.dest);
});

// clear fonts folder for new fonts
gulp.task('clean:fonts', () => {
  return del.sync(fonts.dest);
});

gulp.task( 'devjs', [ 'devpublicjs', 'devadminjs', 'devlocalizejs' ]);

// concat and copy js files
gulp.task('devpublicjs', (cb) => {
  pump([
    gulp.src(js.source[0]),
      deporder(),
      concat(js.filename[0]),
      gulp.dest(js.dest),
      (browserSync !== false ? browserSync.reload({stream: true}) : gutil.noop())
    ], cb);
});
// concat and copy js files
gulp.task('devadminjs', (cb) => {
  pump([
    gulp.src(js.source[1]),
      deporder(),
      concat(js.filename[1]),
      gulp.dest(js.dest),
      (browserSync !== false ? browserSync.reload({stream: true}) : gutil.noop())
    ], cb);
});
// concat and copy js files
gulp.task('devlocalizejs', (cb) => {
  pump([
    gulp.src(js.source[2]),
      deporder(),
      concat(js.filename[2]),
      gulp.dest(js.dest),
      (browserSync !== false ? browserSync.reload({stream: true}) : gutil.noop())
    ], cb);
});

// strip debug codes and minify js file into on file
gulp.task('stripjsdebug', function(cb){
  pump([
    gulp.src(js.source[0]),
      deporder(),
      concat(js.filename[0]),
      gulpIf(js.type, uglify()),
      stripdebug(),
      gulp.dest(js.dest),
      (browserSync !== false ? browserSync.reload({stream: true},function(err, bs) {
        console.log(bs.options.urls.local);
      }) : gutil.noop())
  ],
cb );
});