'use strict';
/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */


( function() {
	var container, sidebar, sidebarBtn, closeSidebarBtn, button, closeBtn, menu, links, i, len;

	container = document.getElementById( 'site-navigation' );
	if ( ! container ) {
		return;
	}

	sidebar = document.getElementById( 'secondary' );
	if ( ! sidebar ) {
		return;
	}

	button = container.getElementsByTagName( 'button' )[0];
	if ( 'undefined' === typeof button ) {
		return;
	}

	sidebarBtn = container.getElementsByTagName( 'button' )[1];
	if ( 'undefined' === typeof sidebarBtn ) {
		return;
	}

	closeBtn = document.getElementById( 'btn-close' );
	if ( 'undefined' === typeof closeBtn ) {
		return;
	}

	closeSidebarBtn = document.getElementById( 'btn-close-sidebar' );
	if ( 'undefined' === typeof closeSidebarBtn ) {
		return;
	}

	menu = container.querySelector( '#navigation-menus' );

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	menu.setAttribute( 'aria-expanded', 'false' );
	if ( -1 === menu.className.indexOf( 'nav-menu' ) ) {
		menu.className += ' nav-menu';
	}

  closeSidebarBtn.onclick = function() {
	if ( hasClass( sidebar, 'toggled' ) === true ) {
		toggleClass( sidebar, 'toggled' );
		sidebarBtn.setAttribute( 'aria-expanded', 'false' );
	} else {
		toggleClass( sidebar, 'toggled' );
		sidebarBtn.setAttribute( 'aria-expanded', 'true' );
	}
  };

  sidebarBtn.onclick = function() {
	if ( hasClass( sidebar, 'toggled' ) === true ) {
		toggleClass( sidebar, 'toggled' );
		sidebarBtn.setAttribute( 'aria-expanded', 'false' );
	} else {
		if ( hasClass( container, 'toggled' ) ) {
			toggleClass( container , 'toggled');
		}
		toggleClass( sidebar, 'toggled' );
		sidebarBtn.setAttribute( 'aria-expanded', 'true' );
	}
  };

  closeBtn.onclick = function() {
	if ( -1 !== container.className.indexOf( 'toggled' ) ) {
		container.className = container.className.replace( ' toggled', '' );
		button.setAttribute( 'aria-expanded', 'false' );
		menu.setAttribute( 'aria-expanded', 'false' );
	} else {
		container.className += ' toggled';
		button.setAttribute( 'aria-expanded', 'true' );
		menu.setAttribute( 'aria-expanded', 'true' );
	}
  };

  button.onclick = function() {
    if ( -1 !== container.className.indexOf( 'toggled' ) ) {
      container.className = container.className.replace( ' toggled', '' );
      button.setAttribute( 'aria-expanded', 'false' );
      menu.setAttribute( 'aria-expanded', 'false' );
    } else {
		if ( hasClass( sidebar, 'toggled' ) ) {
			toggleClass( sidebar , 'toggled');
		}
      container.className += ' toggled';
      button.setAttribute( 'aria-expanded', 'true' );
      menu.setAttribute( 'aria-expanded', 'true' );
    }
  };

	// Get all the link elements within the menu.
	links    = menu.getElementsByTagName( 'a' );

	// Each time a menu link is focused or blurred, toggle focus.
	for ( i = 0, len = links.length; i < len; i++ ) {
		links[i].addEventListener( 'focus', toggleFocus, true );
		links[i].addEventListener( 'blur', toggleFocus, true );
	}

	/**
	 * 
	 * 
	 * Sets or removes .focus class on an element.
	 * 
	 * 
	 */
	function toggleFocus() {
		var self = this;

		// Move up through the ancestors of the current link until we hit .nav-menu.
		while ( -1 === self.className.indexOf( 'nav-menu' ) ) {

			// On li elements toggle the class .focus.
			if ( 'li' === self.tagName.toLowerCase() ) {
				if ( -1 !== self.className.indexOf( 'focus' ) ) {
					self.className = self.className.replace( ' focus', '' );
				} else {
					self.className += ' focus';
				}
			}

			self = self.parentElement;
		}
	}

	/**
	 * 
	 * Toggles `focus` class to allow submenu access
	 * on tablets.
	 * 
	 * 
	 */
	( function( container ) {
		var touchStartFn, i,
			parentLink = container.querySelectorAll( '.menu-item-has-children > a, .page_item_has_children > a' );

		if ( 'ontouchstart' in window ) {
			touchStartFn = function( e ) {
				var menuItem = this.parentNode, i;

				if ( ! menuItem.classList.contains( 'focus' ) ) {
					e.preventDefault();
					for ( i = 0; i < menuItem.parentNode.children.length; ++i ) {
						if ( menuItem === menuItem.parentNode.children[i] ) {
							continue;
						}
						menuItem.parentNode.children[i].classList.remove( 'focus' );
					}
					menuItem.classList.add( 'focus' );
				} else {
					menuItem.classList.remove( 'focus' );
				}
			};

			for ( i = 0; i < parentLink.length; ++i ) {
				parentLink[i].addEventListener( 'touchstart', touchStartFn, false );
			}
		}
	}( container ) );

  /**
   * stop first links from action
   * 
   * 
   * 
   * 
   */
	( function( container ) {
		if ( document.addEventListener ) {
			container.addEventListener( 'click', prevent );
		} else { container.attachEvent( 'click', prevent );	}
	} )( container );

	/**
	 * adding the adminbar class for mading theme
	 * responsivable with when admin is logged in
	 * 
	 * 
	 */

	( function( container ) {
		document.onreadystatechange = function ( ) {

			if (document.readyState === 'complete') {

				
			if ( ! isInPage( '#wpadminbar' ) ) {
				return;
			}

			toggleClass(document.body, 'hasadminbar');
			}
		}
	} ) ( container );


	(function ( container ) {

		var _userEvent = ( 'ontouchstart' in window ? 'touchend' : 'click' );
		if ( document.addEventListener ) {
			container.addEventListener( _userEvent, SlideMenu );
			container.addEventListener( _userEvent, waveThis );
		} else if ( document.attachEvent ) {
			container.attachEvent( _userEvent, SlideMenu );
			container.attachEvent( _userEvent, waveThis );
		} else {
			container.ontouchend = function (e) { e.preventDefault(); e.target.click();};
			container.onclick = SlideMenu;
			container.onclick = waveThis;
		}
	} ) ( menu ) ;

	/**
	 * Pin Navigation bar to Bottom of the page
	 */

	(function ( container ) {

		var _userEvent = ( 'ontouchstart' in window ? 'touchend' : 'click' );
		if ( document.addEventListener ) {
			container.addEventListener( _userEvent, slideNavBar );
		} else if ( document.attachEvent ) {
			container.attachEvent( _userEvent, slideNavBar );
		} else {
			container.ontouchend = function (e) { e.preventDefault(); e.target.click();};
			container.onclick = slideNavBar;
		}
	} ) ( container.querySelector( 'button.navigation-slider' ) ) ;
	

} )( );
