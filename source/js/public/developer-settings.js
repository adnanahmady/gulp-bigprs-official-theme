/**
 * 
 * javascript codes that are aparts
 * in the pages
 * 
 */

( function () {
  var _form, _fullWidthBtn, _closeBtn, _btnEvent, container;
  _btnEvent = ( 'ontouchstart' in window ? 'touchend' : 'click' );

	container = document.getElementById( 'site-navigation' );
	if ( ! container ) {
		return;
	}

  _fullWidthBtn = document.querySelector( '#btn-full-width' );
  if ( 'undefined' === typeof _fullWidthBtn ) {
    return;
  }

  _form = document.getElementsByClassName( 'search-form' );
  if ( _form.length === 0 ) {
    return;
  }

  _closeBtn = document.querySelectorAll( '[role="alert"]' );
  if ( _closeBtn === 0 ) {
    return;
  }

  for ( i = 0; i < _form.length; i ++ ) {
    if ( document.addEventListener ) {
      _form[i].querySelector( 'SELECT' ).addEventListener( 'change', addFormChild );
    } else if ( document.attachEvent ) {
      _form[i].querySelector( 'SELECT' ).attachEvent( 'change', addFormChild );
    } else {
      _form[i].querySelector( 'SELECT' ).onchange = addFormChild;
    } 
  }

  for ( i = 0; i < _closeBtn.length; i ++ ) {
    if ( document.addEventListener ) {
      _closeBtn[i].querySelector( '[role="close-btn"]').addEventListener( _btnEvent, addClass );
    } else if ( document.attachEvent ) {
      _closeBtn[i].querySelector( '[role="close-btn"]').attachEvent( _btnEvent, addClass );
    } else {
      _closeBtn[i].querySelector( '[role="close-btn"]').ontouchend = function(event) {event.preventDefault(); this.target.click(); }
      _closeBtn[i].querySelector( '[role="close-btn"]').onclick = addClass;
    }
  }

  ( function ( _btn ) {
    if ( document.addEventListener ) {
      _btn.addEventListener( _btnEvent, fullWidthSidebar );
    } else if ( document.attachEvent ) {
      _btn.attachEvent( _btnEvent, fullWidthSidebar );
    } else {
      _btn.ontouchend = function(event) {event.preventDefault(); this.target.click(); }
      _btn.onclick = fullWidthSidebar;
    }
  } ) ( _fullWidthBtn );

} ) ( );  
	/**
	 * 
	 * add close.svg image to the links
	 * that has children
	 * 
	 * 
	 */
function  addimg( ) {
  var child, root, el, i, len, container;
  container = document.getElementById( 'site-navigation' );
  if ( ! container ) {
    return;
  }
  root = gobj.root;
  el = container.querySelectorAll( '.menu-item-has-children > a' );

  for ( i = 0, len = el.length; i < len; i ++ ) {

    child = document.createElement( 'IMG' );
    child.src = document.addEventListener ? root + '/images/close.svg': root + '/images/close.png';
    child.setAttribute('class', ' after');
    child.alt = gobj.menuItemHasChildren;
    el[i].appendChild(child);

  }
}
  
function prev( container ) {
  container = document.getElementById( 'site-navigation' );
  if ( ! container ) {
    return;
  }
  if ( document.addEventListener ) {
    container.addEventListener( 'click', prevent );
  } else { container.attachEvent( 'click', prevent );	}
}

addimg();
prev();