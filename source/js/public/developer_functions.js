/**
 * File developer_functions.js.
 *
 * Handles Developer Functions
 *
 */

/**
 * output @param text in console
 * @param text
 */

function echo ( text ) {
  console.log( text );
}

function is_a(target) {
  if (target.tagName == 'A') {
    return true;
  } return false;
}

function is_img(target) {
  if (target.tagName == 'IMG') {
    return true;
  } return false;
}

function has_ul(target) {
  for(var i = 0; i < target.childNodes.length; i ++) {
    if (target.childNodes[i].nodeName == 'UL') {
      return true;
    }
  } return false;
}

function isInPage(node) {
  return document.querySelector(node);
}

function toggleClass ( target, _class ) {
  if ( target.className.indexOf( ' ' + _class ) > -1 ) {
    target.className = target.className.replace( ' ' + _class, '' );
  } else {
    target.className += ' ' + _class;
  }
}

function hasClass ( _element, _class ) {
  var _elementClass;
  try {
    _elementClass = _element.className;
  } catch ( e ) {
    _elementClass = undefined;
   }
  // if ( null === _element.className ) { throw ( 'false' ); return false;}
  // var _elementClass = undefined === _element.className ? '' : _element.className;
  if ( ( _elementClass !== undefined ) && _element.className.indexOf ( ' ' + _class ) > -1 )
  { return true; } return false;
}

function prevent(event) {
  var element = event.target;
  if (is_a(element)) {
    element = element.parentElement;
  } else if ( is_img( element ) ) {
    element = element.parentElement.parentElement;
  }
  if (has_ul(element)) {
    event.preventDefault();
  }
}

function HasClassIn ( _element, _class, _stopParentClass ) {
  while ( hasClass ( _element, _stopParentClass ) === false ) {
    if ( hasClass ( _element, _class ) === false ) {
      _element = _element.parentElement;
    } else { return true; }
  } return false;
}

function getParentByClass ( _element, _class ) {
  _element = _element.parentElement;
  while ( ( _element.tagName == 'BODY' ) === false ) {
    if ( hasClass ( _element, 'menu-item' ) === false ) {
      _element = _element.parentElement;
    } else { return _element; }
  } return undefined;
}
function getElementInPathByTagName ( _element, _tagname ) {
  while ( ( _element.tagName == 'BODY' ) === false ) {
    if ( _element.tagName === _tagname.toUpperCase() ) {
      _element = _element.parentElement;
    } else { return _element; }
  } return undefined;
}

function getCount ( _elementId ) {
  var _len = 0, row, _element;

  _element = document.querySelector ( '#' + _elementId ).querySelectorAll ( '.active' );
  for ( row = 0; row < _element.length; row ++ ) {
    _len += _element[row].children.length;
  }
  return _len;
}

function getHeight ( _element ) {
  var _height = 0;

  _height += _element.getBoundingClientRect().height;

  // _elementa = _element.querySelector ( '.active' );
  // if ( !_elementa ) {
  //   _elementa = 0;
  // } else { _elementa = _elementa.children.legnth;}

  // _height *= ( _element.querySelector ( 'ul' ).children.length + _elementa );
  _height *= ( _element.querySelector ( 'ul' ).children.length + getCount( _element.id ) );

  return _height;
}

function getAllHeight ( _element ) {
  var _height = 0, _loop;
      // echo ( 'sub height : ' + _element.querySelector( 'li' ).getBoundingClientRect().height );
  if ( _element.querySelector ( 'ul' ) ) {
    if (_element.querySelector('ul').getBoundingClientRect().height == 0) {
      _loop = _element.querySelectorAll ( 'li' );
      if ( _loop[0].getBoundingClientRect().height > 0 ) {
        for ( var i = 0; i < _loop.length; i ++ ) {
            _height += _loop[i].getBoundingClientRect().height;
        } 
      } else {
        _height += ( _element.getBoundingClientRect().height  );
  
        _height *= (_element.querySelectorAll('li').length);
      }
    } else {
      _height = _element.querySelector('ul').getBoundingClientRect().height;
    }
  }
  return _height;
}

var addRule = ( function ( style ) {
  var sheet = document.head.appendChild ( style ).sheet;
  return function ( selector, css ) {
    var properties = Object.keys( css ).map ( function ( key ) {
      return key + ':' + css[key]
    }).join( ';' );
    sheet.insertRule( selector + '{' + properties + '}', sheet.cssRules.length );
  }
} ) ( document.createElement ( 'STYLE' ) );

function changeParentsMaxHeight ( parent, _class, _stopParentClass, _menuHeight, Sign ) {
  function _Sign ( Sign ) {
    if ( Sign === '-' ) { return ( _height - _menuHeight); }
    return ( _height + _menuHeight);
  }
  while ( hasClass ( parent, _stopParentClass ) === false ) {
    if ( hasClass ( parent, _class ) === true ) {
      
        var parentId = parent.id;
        _height = getHeight ( parent );
        addRule ( '#' + parentId + '.active > ul', {
          'max-height': _Sign( Sign ) + 'px !important'
        } );
    } parent = parent.parentElement;
  }
}

function SlideMenu ( event ) {
  var _element, _menuHeight;
  if ( event.target ) { _element = event.target; }
  else if ( event.srcElement ) { _element = event.srcElement; }
  else { _element = this; }
  if ( HasClassIn( _element, 'menu-item-has-children', 'nav-menu' ) !== false ) {
    _element = getParentByClass( _element, 'menu-item' );
    _menuHeight = getAllHeight( _element );
      addRule ( '#' + _element.id + '.active > ul', {
        'max-height': _menuHeight + 'px !important'
        // ,'height': _menuHeight + 'px !important'
        // , display: 'block !important'
      } );

    // if ( window.innerWidth > 720 ) {

    //   addRule ( '#' + _element.id + '.active > ul', {
    //     'max-height': _menuHeight + 'px !important'
    //     ,'height': _menuHeight + 'px !important'
    //     // , display: 'block !important'
    //   } );
    // } else {

    // }
    
    toggleClass( _element, 'active');
    
    /**
     * 
     * this part is working 
     * just slow system down
     * 
     * 
     */
    // if ( hasClass ( _element, 'active' ) === true ) {

    //   changeParentsMaxHeight( _element, 'active', 'nav-menu', _menuHeight, '+' );

    //   addRule ( '#' + _element.id + '.active > ul', {
    //     'max-height': _menuHeight + 'px !important',
    //     display: 'block !important'
    //   } );

    // } else {
    //   changeParentsMaxHeight( _element, 'active', 'nav-menu', _menuHeight, '-' );
    // }
  }
}

function getPosition ( event ) {
  var _mouse = { Left: 0, Top: 0 },_touch;
    if ( event.type == 'touchstart' || event.type == 'touchmove' || event.type == 'touchend' || event.type == 'touchcancel' ) {
      var _touch = event.touches[0] || event.changedTouches[0];
      _mouse.Left = _touch.clientX;
      _mouse.Top  = _touch.clientY;
    } else {
      _mouse.Left = event.clientX;
      _mouse.Top  = event.clientY;
    }
    return _mouse;
}

function waveThis ( event ) {
  var _elementId, _element, _mouse, _rect;
  if ( event.target ) { _element = event.target; }
  else if ( event.srcElement ) { _element = event.srcElement; }
  else { _element = this; }
  if ( HasClassIn( _element, 'menu-item', 'nav-menu' ) === true ) {
    _element = getParentByClass ( _element, 'menu-item' );
    _elementId = _element.id;
    _element = _element.querySelector ( 'a' );
      _rect = _element.getBoundingClientRect();
      _mouse = getPosition( event );
      _mouse.Left = _mouse.Left - _rect.left;
      _mouse.Top = _mouse.Top - _rect.top;
    addRule ( '#' + _elementId + ' > a.wave:after', {
      left: _mouse.Left + 'px !important',
      top: _mouse.Top + 'px !important'
    } );

    if ( hasClass ( _element, 'wave' ) ) {
      toggleClass( _element, 'wave');
    }
    setTimeout ( function () { toggleClass( _element, 'wave') }, 0 );
  }
}

function addFormChild( event ) {
  if ( this.value === 'product' ) {
    hidden        = document.createElement( 'INPUT' );
    hidden.type   = 'hidden';
    hidden.name   = 'post_type';
    hidden.value  = 'product';
    hidden.id     = 'bigprs-product-type';
    this.parentElement.appendChild( hidden );
  } else {
    hidden        = document.getElementById( 'bigprs-product-type' );
    if ( 'null' != hidden && 'undefined' != hidden ) {
      this.parentElement.removeChild( hidden );
    }
  }
}

var toggle = ( function () {
  var toggle = false;
  return function() {
    if ( toggle === false ) { return ( toggle = true ) ; }
    else { return ( toggle = false ); }
  }
} ) ( );

function fullWidthSidebar ( event ) {
  var fullwidth, b = this.querySelector( 'IMG' );
  if ( toggle() ) {
    b.setAttribute( 'onerror', b.getAttribute('onerror').replace( /full-width/gi, 'less-width' ) );
    b.setAttribute( 'src', b.getAttribute('src').replace( /full-width/gi, 'less-width' ) );
    this.querySelector( 'span' ).innerHTML = gobj.lessScreenText;
    this.removeChild( this.querySelector( 'IMG' ) );
    this.appendChild( b );
  } else {
    b.setAttribute( 'onerror', b.getAttribute('onerror').replace( /less-width/gi, 'full-width' ) );
    b.setAttribute( 'src', b.getAttribute('src').replace( /less-width/gi, 'full-width' ) );
    this.querySelector( 'span' ).innerHTML = gobj.fullScreenText;
    this.removeChild( this.querySelector( 'IMG' ) );
    this.insertBefore( b, this.querySelector( 'SPAN' ) );
  }
  fullwidth = document.querySelector( '.widget-area.toggled' );
  if ( 'undefined' !== fullwidth ) {
    toggleClass( fullwidth, 'full-screen')
  }
}

function getParent( _element, _query ) {
  var _return_element = _element;
  while( _element.tagName !== 'BODY' ) {
    if ( null !== _element.parentElement.querySelector( _query ) ) {
      return _element;
    } else {
      _element = _element.parentElement;
    }
  }
  return _return_element;
}

function addClass ( event ) {
  var parent = getParent(event.target, '[role="alert"]');
  toggleClass(parent, "closed");
}

/**
 * Toggle Navigation bar class 
 * on the page 
 * for Making Effects
 */
function slideNavBar ( event ) {
  toggleClass( getParent( this, 'nav.main-navigation' ), 'pin-menu-bar' );
}