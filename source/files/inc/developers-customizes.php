<?php

/**
 * Add a custom link to the end of a specific menu that uses the wp_nav_menu() function
 */
add_filter('wp_nav_menu_items', 'bigprs_add_custom_logo', 10, 2);
function bigprs_add_custom_logo($items, $args){
	if ($args->theme_location == 'quick-access-menu' && !empty($items)) {
		$additions = [
			'<li class="has-btn d-inline-block v-top"><button id="btn-close" class="close-this btn-dark text-light btn btn-lg v-top" area-label="close main menu area">'.
			'<img src="' . get_template_directory_uri() . '/images/close.svg" onerror=\'src="' . get_template_directory_uri() . '/images/close.svg"\' class="img pl-1" alt="close button">' .
			__( 'close', 'bigprs-official') . '</button></li>',
			'<li class="is-item d-inline-block v-top"><div class="custom-logo small-logo">' . get_custom_logo() . '</div></li>'
		];
		$items = add_items($additions) . $items;
	}
	return $items;
	// return ['hcousadhfukwefuse'];
}
/**
 * add custom class's to custom logo
 */
add_filter( 'wp_get_attachment_image_attributes', 'bigprs_add_class_to_custom_logo');
function bigprs_add_class_to_custom_logo( $attr ) {
	if ( isset( $attr[ 'class' ] ) && 'custom-logo' === $attr[ 'class' ] ) {
		$attr[ 'class' ] = 'custom-logo';
	} return $attr;
}

wp_register_script( 'bigprs_localize_scripts', get_template_directory_uri() . '/js/localize.js', array(), '13961220', true );
wp_localize_script(
	'bigprs_localize_scripts',
	'gobj', 
	array(
		'homeUrl'	=> esc_url( home_url() ),
		'root'	=> esc_url( get_template_directory_uri() ),
		'fullScreenText'  => __('Full', 'bigprs-official'),
		'lessScreenText'  => __('Less', 'bigprs-official'),
		'menuItemHasChildren' => __('list item has children', 'bigprs-official')
	) );
wp_enqueue_script( 'bigprs_localize_scripts' );



/**
 * Filters the archive link content.
 *
 * @since 2.6.0
 * @since 4.5.0 Added the `$url`, `$text`, `$format`, `$before`, and `$after` parameters.
 *
 * @param string $link_html The archive HTML link content.
 * @param string $url       URL to archive.
 * @param string $text      Archive text description.
 * @param string $format    Link format. Can be 'link', 'option', 'html', or custom.
 * @param string $before    Content to prepend to the description.
 * @param string $after     Content to append to the description.
 */
add_filter( 'get_archives_link', 'bigprs_change_archives_link', 10, 6);
function bigprs_change_archives_link($link_html, $url, $text, $format = 'html', $before = '', $after = '') {
	$text = wptexturize($text);
	$url = esc_url($url);

	if ('link' == $format)
		$link_html = "\t<link rel='archives' title='" . esc_attr( $text ) . "' href='$url' />\n";
	elseif ('option' == $format)
		$link_html = "\t<option value='$url'><span class='menu-before'>$before</span> $text <span class='menu-after'>$after</span></option>\n";
	elseif ('html' == $format)
		$link_html = "\t<li><span class='menu-before'>$before</span><a href='$url'>$text <span class='menu-after'>$after</span></a></li>\n";
	else // custom
		$link_html = "\t<span class='menu-before'>$before</span><a href='$url'>$text <span class='menu-after'>$after</span></a>\n";

	return $link_html;
}

/**
 * 
 * add span around parantesis
 * 
 * with menu-class for Effecting
 * 
 * CSS on it
 * 
 * after <A> element
 * 
 * 
 */
add_filter( 'wp_list_categories', 'bigprs_change_categories_link', 10, 2 );
function bigprs_change_categories_link( $output, $args ) {
	if ( $args[ 'show_count' ] > 0 ) {
		$output = str_replace( '</a> (', '<span class="menu-after">(', $output );
		$output = str_replace( ')', ')</span></a>', $output );
	} else if ( $args[ 'dropdown' ] > 0 ) {
		$output = str_replace( '  (', '<span class="menu-after">(', $output );
		$output = str_replace( ')', ')</span></a>', $output );
	}
	return $output;
}

/**
 * remove address fields from billing form
 * ref - https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 */
add_filter( 'woocommerce_billing_fields', 'bigprs_custom_billing_fields' );
function bigprs_custom_billing_fields( $fields = array() ) {
	unset( $fields[ 'billing_company'		]);
	unset( $fields[ 'billing_address_1'	]);
	unset( $fields[ 'billing_address_2' ]);
	unset( $fields[ 'billing_state' 		]);
	unset( $fields[ 'billing_city' 			]);
	unset( $fields[ 'billing_phone' 		]);
	unset( $fields[ 'billing_postcode' 	]);
	unset( $fields[ 'billing_country' 	]);
	return $fields;
}

/**
 * remove some fields from billing form
 * Our hooked in function - $fields is passed via the filter!
 * Get all the fields - https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 */
add_filter( 'woocommerce_checkout_fields', 'bigprs_custom_override_checkout_fields' );
function bigprs_custom_override_checkout_fields( $fields ) {
	unset( $fields[ 'billing' ][ 'billing_company' 		] );
	unset( $fields[ 'billing' ][ 'billing_address_1' 	] );
	unset( $fields[ 'billing' ][ 'billing_postcode' 	] );
	unset( $fields[ 'billing' ][ 'billing_state' 			] );
	return $fields;
}
//add_filter ( 'widget_categories_dropdown_args',
//function ($cat_args, $instance) {
//
//	$output = str_replace( '(', '<span class="menu-count">(', $output );
//	$output = str_replace( ')', ')</span></a>', $output );
//
//	return $cat_args;
//} , 10, 2 );
function bigprs_modify_read_more_link( $link ) {
	$link = preg_replace( '|class="more-link|', 'class="continue-link btn btn-primary btn-md d-block', $link );
	return $link;
}
add_filter( 'the_content_more_link', 'bigprs_modify_read_more_link' );