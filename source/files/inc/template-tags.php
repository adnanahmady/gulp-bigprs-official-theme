<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Bigprs_Official_theme
 */

if ( ! function_exists( 'bigprs_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function bigprs_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			'<img src="' . get_template_directory_uri() . '/images/posted-on.svg" onerror="src=\'' . get_template_directory_uri() . '/images/posted-on.png\'" width="24" height="24" alt="%2$s" title="%2$s" class="img ml-1 v-middle">' .
			esc_html_x( 'Posted on %1$s', 'post date', 'bigprs-official' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>',
			sprintf( ' %s ' . __( 'create time', 'bigprs-official' ), get_the_title() )
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'bigprs_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function bigprs_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			'<img src="' . get_template_directory_uri() . '/images/posted-by.svg" onerror="src=\'' . get_template_directory_uri() . '/images/posted-by.png\'" width="24" height="24" alt="%2$s" title="%2$s" class="img ml-1 v-middle">' .
			esc_html_x( 'by %1$s', 'post author', 'bigprs-official' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>',
			sprintf( __( 'Author of', 'bigprs-official' ) . ' %s ', get_the_title() )
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'bigprs_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function bigprs_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ' ', 'bigprs-official' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links"><img src="' . get_template_directory_uri() . '/images/posted-in.svg" onerror="src=\'' . get_template_directory_uri() . '/images/posted-in.png\'" width="24" height="24" alt="%2$s" title="%2$s" class="img ml-1 v-middle">' . esc_html__( 'Posted in ', 'bigprs-official' ) . ' %1$s</span>', 
				$categories_list,
				sprintf( __( 'categories of', 'bigprs-official' ) . ' %s ', get_the_title() )
			); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ' ', 'list item separator', 'bigprs-official' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links"><img src="' . get_template_directory_uri() . '/images/post-tags.svg" onerror="src=\'' . get_template_directory_uri() . '/images/post-tags.png\'" width="24" height="24" alt="%2$s" title="%2$s" class="img ml-1 v-middle">' . esc_html__( 'Tagged ', 'bigprs-official' ) . ' %1$s</span>', 
				$tags_list,
				sprintf( __( 'Tages for', 'bigprs-official' ) . ' %s ', get_the_title() )
			 ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">', sprintf('<img src="%1$s/images/comment.svg" onerror="src=\'%1$s/images/comment.png\'" width="24" height="24" alt="%2$s" title="%2$s" class="img ml-1 v-middle">',
				get_template_directory_uri(),
				sprintf( __( 'Comments on', 'bigprs-official' ) . ' %s ',	get_the_title() )
			);
			comments_popup_link(
				sprintf( wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'bigprs-official' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf('<img src="' . get_template_directory_uri() . '/images/edit-post.svg" onerror="src=\'' . get_template_directory_uri() . '/images/edit-post.png\'" width="24" height="24" alt="%2$s" title="%2$s" class="img ml-1 v-middle">' . wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit', 'bigprs-official' ) . '<span class="screen-reader-text">%s</span>',
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title(),
				sprintf( __( 'Edit', 'bigprs-official' ) . ' %s ', get_the_title() )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'bigprs_post_thumbnail' ) ) :
/**
 * Displays an optional post thumbnail.
 *
 * Wraps the post thumbnail in an anchor element on index views, or a div
 * element when on single views.
 */
function bigprs_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail('bigprs_shop_medium'); ?>
	</div><!-- .post-thumbnail -->

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php
			the_post_thumbnail( 'bigprs_shop_medium', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
		?>
	</a>

	<?php endif; // End is_singular().
}
endif;
