<?php

	/**
	 *
	 * this function will concatinate the array of input
	 * and return it as an array
	 *
	 * @param array $items
	 *
	 * @return stringa
	 */

	function add_items( $items = []) {

		$return = '';
		foreach ($items as $value ) {
			if ( ! empty($value) ) {
			$return .= $value;
			}
		}
		return $return;
	}

	function bigprs_footer_menu ( ) {
		wp_nav_menu( array(
			'theme_location'    => 'footer-menu',
			'container'   			=> 'nav',
			'container_id'		 	=> 'bigprs-official-footer-menu-container',
			'container_class' 	=> 'bigprs-official-footer-menu-container',
			'menu'							=> 'ul',
			'menu_id'     			=> 'bigprs-official-footer-menu-bar',
			'menu_class'  			=> 'bigprs-official-footer-menu-bar',
			'depth'							=> 1,
		) );
	}

	function bigprs_quick_access_menu ( ) {
		wp_nav_menu( array(
			'theme_location'    => 'quick-access-menu',
			'container'   			=> '',
			'menu'							=> '',
			'menu_id'						=> 'h-access-menu',
			'menu_class'  			=> 'h-access-menu p-0 m-0',
			'link_before' 			=> '<span class="screen-reader-text">',
			'link_after'				=> '</span>',
			'depth'							=> 1,
		) );
	}