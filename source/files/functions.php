<?php
/**
 * Bigprs Official theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Bigprs_Official_theme
 */

if ( ! function_exists( 'bigprs_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bigprs_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Bigprs Official theme, use a find and replace
		 * to change 'bigprs-official' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bigprs-official', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Add post thumbnails sizes
		 *
		 */
		//Default WordPress
		the_post_thumbnail( 'thumbnail' );     // Thumbnail (150 x 150 hard cropped)
		the_post_thumbnail( 'medium' );        // Medium resolution (300 x 300 max height 300px)
		the_post_thumbnail( 'medium_large' );  // Medium Large (added in WP 4.4) resolution (768 x 0 infinite height)
		the_post_thumbnail( 'large' );         // Large resolution (1024 x 1024 max height 1024px)
		the_post_thumbnail( 'full' );          // Full resolution (original size uploaded
		//With WooCommerce
		the_post_thumbnail( 'shop_thumbnail' ); // Shop thumbnail (180 x 180 hard cropped)
		the_post_thumbnail( 'shop_catalog' );   // Shop catalog (300 x 300 hard cropped)
		the_post_thumbnail( 'shop_single' );
		// custom thumbnail sizes
		the_post_thumbnail( array ( 100, 100 ) );
		the_post_thumbnail( 'bigprs_shop_medium' );
		the_post_thumbnail( 'bigprs_shop_mini_cart' );

		// the custom post thumbnail sizes
		add_image_size( 'bigprs_shop_medium', 450, 450);
		add_image_size( 'bigprs_shop_mini_cart', 50, 50 );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'bigprs-official' ),
			'footer-menu' => esc_html__( 'Footer Menu', 'bigprs-official' ),
			'quick-access-menu'		=> esc_html__( 'Quick Access Menu', 'bigprs-official' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'message'
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'bigprs_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'bigprs_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bigprs_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bigprs_content_width', 640 );
}
add_action( 'after_setup_theme', 'bigprs_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bigprs_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'bigprs-official' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add Widgets here to show on Site Footer.', 'bigprs-official' ),
		'before_widget' => '<section id="%1$s" class="footer-widtget widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bigprs-official' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'bigprs-official' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'bigprs_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bigprs_scripts() {
	wp_enqueue_style( 'bigprs-style', get_stylesheet_uri(), array(), '13970121' );

	wp_enqueue_style( 'bigprs-sidebar-content', get_stylesheet_directory_uri() . '/layouts/sidebar-content.css');

	// wp_enqueue_script( 'bigprs-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	// wp_enqueue_script( 'bigprs-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'bigprs_all_scripts', get_template_directory_uri() . '/js/script.min.js', array(), '20181702', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bigprs_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

	/**
	 * Load Developers custom functions
	 */
	require get_template_directory() . '/inc/developers-functions.php';

	/**
	 * Load Developers customizes
	 */
	require get_template_directory() . '/inc/developers-customizes.php';

	/**
	 * Load Changed Widgets
	 */

	require get_template_directory() . '/inc/bigprs-widget-recent-comments.php';

	require get_template_directory() . '/inc/bigprs_recent_posts_class.php';

