<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label for="searchform-<?php echo $randId = wp_rand(); ?>">
        <span class="screen-reader-text">
            <?php echo __( 'Looking for: ', 'bigprs-official' ); ?>
        </span>
    </label>

    <input id="searchform-<?php echo $randId; ?>" type="search" class="search-field" placeholder="<?php echo __( 'Search for ...', 'bigprs-official' ); ?>" value="<?php the_search_query() ?>" name="s">

    <select id="search-select-<?php echo $randId; ?>">
        <option value="default" <?php 
        if ( filter_input( INPUT_GET, 'post_type' ) === null || filter_input( INPUT_GET, 'post_type' ) !== 'product' ) :
            echo ' selected="selected"';
        endif; ?>><?php echo __( 'every where', 'bigprs-official' ); ?></option>

        <option value="product" <?php 
        if ( filter_input( INPUT_GET, 'post_type' ) !== null && filter_input( INPUT_GET, 'post_type' ) === 'product' ) :
            echo ' selected="selected"';
        endif; ?>><?php echo __( 'products', 'bigprs-official' ); ?></option>
    </select>

    <button class="search-button btn-light text-light btn btn-lg v-middle" type="submit">

        <img src="<?php echo get_template_directory_uri(); ?>/images/search.svg" onerror="<?php echo get_template_directory_uri(); ?>/images/search.png" alt="<?php echo __( 'search for', 'bigprs-official' ); ?>" title="<?php echo __( 'search for', 'bigprs-official' ); ?>" width="24" height="24" class="search-img">

        <?php echo __( 'search', 'bigprs-official' ); ?>

    </button>

</form> 