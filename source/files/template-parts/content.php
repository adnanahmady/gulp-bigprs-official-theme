<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bigprs_Official_theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php
				bigprs_posted_on();
				bigprs_posted_by();
			?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<?php bigprs_post_thumbnail();
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title"><span>', '</span></h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><span>', '</span></a></h2>' );
		endif; ?>

	<div class="entry-content">
		<?php
			if ((has_excerpt() && !is_singular() && !is_single()) || is_search() && !is_singular() ||  is_category() && !is_singular() || is_archive() && !is_singular() ) :
				the_excerpt();

				printf(
					'<p><a href="%1$s" class="continue-link btn btn-primary btn-md d-block">%3$s<span class="screen-reader-text">"%2$s"</span></a></p>',
					get_permalink(),
					get_the_title(),
					__('more...', 'bigprs-official')
				);
			else:

			the_content( 
				sprintf(
				wp_kses(
					'%3$s<span class="screen-reader-text">"%2$s"</span>',
					array(
						'span' => array(
							'class' => array(),
						),
						'a'     =>  array(
							'class' =>  array()
						)
					)
				),
				get_permalink(),
				get_the_title(),
        __('more...', 'bigprs-official')
			), true
		);
  endif;

	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bigprs-official' ),
		'after'  => '</div>',
	) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php bigprs_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
