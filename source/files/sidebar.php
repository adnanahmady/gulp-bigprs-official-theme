<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bigprs_Official_theme
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<div class="close-btn-area">
        <div class="row">
            <div class="group-area">
                <button id="btn-full-width" class="btn-full-width btn-light text-light btn btn-lg v-top" area-label="close main menu area">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/full-width.svg" onerror="src='<?php echo get_template_directory_uri(); ?>/images/full-width.png'" class="img pl-1" alt="<?php echo __( 'full width button', 'bigprs-official' ); ?>">
                    <span class="btn-prefix prefix-light screen-reader-text"><?php echo __( 'full', 'bigprs-official'); ?></span>
                </button>
                <button id="btn-close-sidebar" class="btn-close-sidebar btn-light text-light btn btn-lg v-top" area-label="close main menu area">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/close-dark.svg" onerror="src='<?php echo get_template_directory_uri(); ?>/images/close-dark.png'" class="img pl-1" alt="<?php echo __( 'close button', 'bigprs-official' ); ?>">
                    <span class="btn-prefix prefix-light"><?php echo __( 'close', 'bigprs-official'); ?></span>
                </button>
            </div>
        </div>
	</div>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside><!-- #secondary -->
