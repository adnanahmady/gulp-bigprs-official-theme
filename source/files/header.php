<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bigprs_Official_theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bigprs-official' ); ?></a>

	
<?php if ( has_header_image() && get_header_textcolor() === 'blank' ) : ?>
	<header id="masthead" class="site-header">
	<?php the_header_image_tag(); ?>
<?php elseif ( has_header_image() && get_header_textcolor() !== 'blank' ) : ?>
	<header id="masthead" class="site-header bg-full-size header-size" style="background-image: url(<?php header_image(); ?>);">
<?php else: ?>
	<header id="masthead" class="site-header">
<?php endif; ?>

<div class="header-custom-logo"><?php the_custom_logo('custom-logo'); ?></div>
		<div class="site-branding">
          <?php if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">

			<button class="menu-toggle btn btn-dark btn-lg btn-sun-glass d-inline" aria-controls="primary-menu" aria-expanded="false"><img src="<?php echo get_template_directory_uri() . '/images/menu.svg'; ?>" onerror="this.src='<?php echo get_template_directory_uri() . '/images/menu.png'; ?>'" class="v-middle img-right" height="32" width="32" alt="<?php echo _e('menu', 'bigprs-official'); ?>"><span class="screen-reader-texts v-middle"><?php echo _e( 'menu', 'bigprs-official'); ?></span></button>


	<?php
		if ( function_exists( 'bigprs_woocommerce_header_cart' ) ) {
			bigprs_woocommerce_header_cart();
		} 
	// 	else {
	// 	printf( '<a href="%1$s" class="shop-menu-button btn btn-dark btn-lg btn-sun-glass d-inline"><img src="%2$s.svg" onerror="src=\'%2$s.png\'" alt="%3$s" title="%3$s" width="24" height="24" class="v-middle img-center"></a>',
	// 	get_permalink( wc_get_page_id( 'shop' ) ),
	// 	get_template_directory_uri() . '/images/shop',
	// 	'alt text' );
	// }
	?>

			<button class="sidebar-toggle btn btn-dark btn-lg btn-sun-glass d-inline" aria-controls="sidebar-area" aria-expanded="false"><span class="screen-reader-texts v-middle"><?php echo _e( 'sidebar', 'bigprs-official'); ?></span><img src="<?php echo get_template_directory_uri() . '/images/menu.svg'; ?>" class="v-middle img-left" onerror="this.src='<?php echo get_template_directory_uri() . '/images/menu.png'; ?>'" height="32" width="32" alt="<?php echo _e('sidebar', 'bigprs-official'); ?>"></button>

			

			<button class="navigation-slider btn btn-dark btn-sm btn-sun-glass d-inline" aria-controls="navigation-bar" aria-expanded="false"><img src="<?php echo get_template_directory_uri() . '/images/pin.svg'; ?>" onerror="this.src='<?php echo get_template_directory_uri() . '/images/pin.png'; ?>'" class="v-middle" height="32" width="32" alt="<?php echo _e('pin navigation bar to bottom of the page', 'bigprs-official'); ?>"><span class="screen-reader-text v-middle"><?php echo _e( 'pin navigation bar to bottom of the page', 'bigprs-official'); ?></span></button>
<!--            <div class="custom-logo small-logo">--><?php //the_custom_logo(); ?><!--</div>-->
			<div class="navigation-menus" id="navigation-menus">
			<?php bigprs_quick_access_menu(); ?>
      <?php 
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'container_class'=> 'bigprs-official-primary-menu-container',
				) );
			?>
			</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
